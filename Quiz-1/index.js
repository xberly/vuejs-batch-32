// soal 1Function Penghitung Jumlah Kata

// Buatlah sebuah function dengan nama jumlah_kata() yang menerima sebuah kalimat (string), dan mengembalikan nilai jumlah kata dalam kalimat tersebut.

// Contoh

// var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
// var kalimat_2 = " Saya Iqbal"
// var kalimat_3 = " Saya Muhammad Iqbal Mubarok "


// jumlah_kata(kalimat_1) // 6
// jumlah_kata(kalimat_2) // 2
// jumlah_kata(kalimat_3) // 4

// *catatan
// Perhatikan double spasi di depan, belakang, maupun di tengah tengah kalimat
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal "
var kalimat_3 = " Saya Muhammad Iqbal Mubarok "
function jumlah_kata(){
    return kalimat_1
  }
   
  var tampung = jumlah_kata();
  console.log(tampung)
  console.log("jumlah_kata " + kalimat_1.length)
  console.log("jumlah_kata " + kalimat_2.length)
  console.log("jumlah_kata " + kalimat_3.length)
 
  


// Function Penghasil Tanggal Hari Esok

// Buatlah sebuah function dengan nama next_date() yang menerima 3 parameter tanggal, bulan, tahun dan mengembalikan nilai tanggal hari esok dalam bentuk string, dengan contoh input dan otput sebagai berikut.
// function next_date(tanggal,bulan,tahun){
//     return kalimat_1
//   }
function next_date(tanggal, bulan,tahun) {
    
    if (tanggal == 29 && bulan == 2 && tahun == 2020) {
        return tanggal - 28 + " Maret "+ tahun
    }else if (tanggal == 28 && bulan == 2 && tahun == 2021) {
        return tanggal - 27 + " Maret "+ tahun
    }else if (tanggal == 31 && bulan == 12 && tahun == 2020) {
        return tanggal - 30 + " Januari 2021"
    }
  
    switch(bulan) {
        case 1: { console.log( ' Januari '); break; }
        case 2: { console.log( ' Februari '); break; }
        case 3: { console.log( ' Maret '); break; }
        case 4: { console.log( ' April '); break; }
        case 5: { console.log( ' Mei '); break; }
        case 6: { console.log( ' Juni '); break; }
        case 7: { console.log( ' Juli '); break; }
        case 8: { console.log( ' Agustus '); break; }
        case 9: { console.log( ' September '); break; }
        case 10: { console.log( ' Oktober '); break; }
        case 11: { console.log( ' November '); break; }
        case 12: { console.log( ' Desember '); break; }
        default: { console.log( 'nihil'); }
        
    }
}
function cetak(tanggal, bulan,tahun){
    console.log(next_date(tanggal , bulan , tahun ))
}
    cetak(29,2,2020)
    cetak(28,2,2021)
    cetak(31,12,2020)

// contoh 1

// var tanggal = 29
// var bulan = 2
// var tahun = 2020

// next_date(tanggal , bulan , tahun ) // output : 1 Maret 2020

// contoh 2

// var tanggal = 28
// var bulan = 2
// var tahun = 2021

// next_date(tanggal , bulan , tahun ) // output : 1 Maret 2021

// contoh 3

// var tanggal = 31
// var bulan = 12
// var tahun = 2020

// next_date(tanggal , bulan , tahun ) // output : 1 Januari 2021

// Catatan :
// 1. Dilarang menggunakan new Date()
// 2. Perhatikan tahun kabisat
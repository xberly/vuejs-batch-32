export const ProfileComponent = {
    data () {
        return {
            profile: {
                id: 1,
                name: 'Muhammad Iqbal Mubarok',
                phone: '(+62) 812-3456-7891',
                address: 'Pyongyang Utara'
            } 
        }
    },
    template: `
        <div>
            <h2>Your Profile</h2>
            <ul>
                <li v-for="(value, key) in profile">
                    {{ key + ' : ' + value }} 
                </li>
            </ul>
        </div>
    ` 
}

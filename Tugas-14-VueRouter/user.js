export const UsersComponent = {
    data () {
        return {
            users: [
                {
                    id: 1,
                    name: 'Mohammed Farrell Eghar Syahputra'
                },
                {
                    id: 2,
                    name: 'Ruby Purwanti'
                },
                {
                    id: 3,
                    name: 'Faqih Muhammad'
                },
                
            ]
        }
    },
    template: `
        <div>
            <h2>Active Users</h2>
            <ul>
                <li v-for="user in users">
                    <router-link :to="'/user/'+user.id"> 
                        {{ user.name }} 
                    </router-link>
                </li>
            </ul>
        </div>
    ` 
}

export const UserComponent = {
    data(){
        return {
            users: [
                {
                    id: 1,
                    name: 'Mohammed Farrell Eghar Syahputra',
                    phone: '(+62) 813-5726-1427',
                    address: 'Jalan Mayjend Sungkono, Bungursari'
                },
                {
                    id: 2,
                    name: 'Ruby Purwanti',
                    phone: '(+62) 895-1453-7878',
                    address: 'Jalan Ki Hajar Dewantara, Bekasi'
                },
                {
                    id: 3,
                    name: 'Faqih Muhammad',
                    phone: '(+62) 896-4646-6969',
                    address: 'Jalan Soekarno Hatta, Jakarta'
                },
                
            ]
        }
    },
    computed: {
        user() {
            return this.users.filter( (user) => {
                return user.id === parseInt(this.$route.params.id)                
            })[0]
        }
    },
    template: `<div >
            <h2>{{ user.name }}</h2>
            <ul>
                <li v-for="(value, key) in user">
                    {{ key +' : '+ value }} <br>
                </li>
            </ul>
        </div>`,
   
}

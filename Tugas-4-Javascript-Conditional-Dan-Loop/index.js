// soal 1

// buatlah variabel seperti di bawah ini

// var nilai;
// pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut. lalu buat lah pengkondisian dengan if-elseif dengan kondisi

// nilai >= 85 indeksnya A
// nilai >= 75 dan nilai < 85 indeksnya B
// nilai >= 65 dan nilai < 75 indeksnya c
// nilai >= 55 dan nilai < 65 indeksnya D
// nilai < 55 indeksnya E
var nilai = 100 
if ( nilai >= 85 ) {
    console.log("indeksnya A")
} else if ( nilai >= 75 && nilai <85 ) {
    console.log("indeksnya B")
}else if ( nilai >= 65 && nilai <75 ) {
    console.log("indeksnya C")
}else if ( nilai >= 55 && nilai <65 ) {
    console.log("indeksnya D")
}else {
    console.log("indeksnya E")
}
// soal 2

// buatlah variabel seperti di bawah ini

var tanggal = 02;
var bulan = 4;
var tahun = 2000;
// ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan buatlah switch case pada bulan, lalu muncul kan string nya dengan output seperti ini 22 Juli 2020 (isi di sesuaikan dengan tanggal lahir masing-masing)
switch(bulan) {
    case 1:   { console.log('january'); break; }
    case 2:   { console.log('feb'); break; }
    case 3:   { console.log('mar'); break; }
    case 4:   { console.log('aprl'); break; }
    default:  { console.log('gadabulan'); }}
// soal 3
// Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi n dan alas n. Looping boleh menggunakan syntax apa pun (while, for, do while).
function segitiga(alas) {
    let hasil = '';
    for (let i = 0; i < alas; i++) {
        for (let j = 0; j <= i; j++) {
            hasil += '#';
        }
        hasil += '\n';
    }
    return hasil;
}
console.log(segitiga(3));
console.log(segitiga(7));
// Output untuk n=3 :

// #
// ##
// ###
// Output untuk n=7 :

// #
// ##
// ###
// ####
// #####
// ######
// #######
// soal 4
// berilah suatu nilai m dengan tipe integer, dan buatlah pengulangan dari 1 sampai dengan m, dan berikan output sebagai berikut.
// contoh :


// Output untuk m = 3

// 1 - I love programming
// 2 - I love Javascript
// 3 - I love VueJS
// ===

// Output untuk m = 5

// 1 - I love programming
// 2 - I love Javascript
// 3 - I love VueJS
// ===
// 4 - I love programming
// 5 - I love Javascript

// Output untuk m = 7

// 1 - I love programming
// 2 - I love Javascript
// 3 - I love VueJS
// ===
// 4 - I love programming
// 5 - I love Javascript
// 6 - I love VueJS
// ======
// 7 - I love programming


// Output untuk m = 10

// 1 - I love programming
// 2 - I love Javascript
// 3 - I love VueJS
// ===
// 4 - I love programming
// 5 - I love Javascript
// 6 - I love VueJS
// ======
// 7 - I love programming
// 8 - I love Javascript
// 9 - I love VueJS
// =========
// 10 - I love programming
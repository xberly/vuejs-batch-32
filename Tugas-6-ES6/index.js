// Soal 1
// buatlah fungsi menggunakan arrow function luas dan keliling persegi panjang dengan arrow function lalu gunakan let atau const di dalam soal ini
const luasPersegiPanjang = (a,b) => {
    let panjang = a;
    let lebar = b;
    return panjang * lebar
}

console.log(luasPersegiPanjang(10,5))
console.log(" ")

const kelilingPersegiPanjang = (lebarKiriKanan, panjangKiriKanan) => {
    return lebarKiriKanan + panjangKiriKanan + lebarKiriKanan + panjangKiriKanan
}

console.log(kelilingPersegiPanjang(10,5))
console.log(" ")
// Soal 2
// Ubahlah code di bawah ke dalam arrow function dan object literal es6 yang lebih sederhana
const newFunction = (firstName, lastName) => {
    const awal = firstName
    const akhir = lastName
    const fullName = {awal, akhir} 
      return awal + " " + akhir
}
    //Driver Code 
console.log(newFunction("William", "Imoh"))
// Soal No 3
// Diberikan sebuah objek sebagai berikut:
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
// dalam ES5 untuk mendapatkan semua nilai dari object tersebut kita harus tampung satu per satu:
// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const address = newObject.address;
// const hobby = newObject.hobby;
// Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)
// Driver code
const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)
console.log(" ")

// soal 4
// Kombinasikan dua array berikut menggunakan array spreading ES6
let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
let combined = [...west, ...east]

//Driver Code
console.log(combined)
console.log(" ")

// soal 5
// sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:
const planet = "earth" 
const view = "glass" 
// var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
const hasil = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(hasil)

var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
const awal_waktu = 10000

readBooksPromise(awal_waktu, books[0])
.then((waktu1) => {
    return readBooksPromise(waktu1, books[1]);
})
.then((waktu2) => {
    return readBooksPromise(waktu2, books[2]);
})
.then((waktu3)=> {
    return readBooksPromise(waktu3, books[3]);
})
.catch(()=>{
})
